//Declare variables as shortcuts
let brow1, brow2, brow3, eye1, eye2, eye3, mouth1, mouth2, mouth3, mouthbutt, eyebutt, browbutt, otherbutt;

//setting up the interaction
current_eye = 1;
current_brow = 1;
current_mouth = 1;

//set up a function which takes direction of clicks to influence the value of features
function eyeChange(){
current_eye = (current_eye - 1 + 3) % 3;
}
function browChange(){
current_brow = (current_brow - 1 + 3) % 3;
}
function mouthChange(){
current_mouth = (current_mouth - 1 + 3) % 3;
}

//library of images
function preload() {
  brow1 = loadImage ('assets/brow1.png');
  brow2 = loadImage ('assets/brow2.png');
  brow3 = loadImage ('assets/brow3.png');
  eye1 = loadImage ('assets/eye1.png');
  eye2 = loadImage ('assets/eye2.png');
  eye3 = loadImage ('assets/eye3.png');
  mouth1 = loadImage ('assets/mouth1.png');
  mouth2 = loadImage ('assets/mouth2.png');
  mouth3 = loadImage ('assets/mouth3.png');
  mouthbutt = loadImage ('assets/mouthbutt.png');
  eyebutt = loadImage ('assets/eyebutt.png');
  browbutt = loadImage ('assets/browbutt.png');

// create an array of shirts and bottoms
eyes = [eye1, eye2, eye3];
brows = [brow1, brow2, brow3];
mouths = [mouth1, mouth2, mouth3];
}

function setup() {
  createCanvas(600, 500);
  background(200, 200, 200);
  console.log("hello world");
}

function draw() {
  background(225, 225, 225);
  ellipseMode(CENTER);
  noStroke();
  fill(222, 188, 83);
  ellipse(250, 250, 425, 425);

  //button placements
  image(browbutt, 525, 150);
  image(eyebutt, 525, 250);
  image(mouthbutt, 525, 350);

  //to load the features
  imageMode(CENTER);
  image(brows[current_brow], 250, 150);
  image(eyes[current_eye], 250, 200);
  image(mouths[current_mouth], 250, 350);
}

function mousePressed () {
   if (mouseX > 475 && mouseX < 575 && mouseY > 100 && mouseY < 200){
     browChange ();
    }

   if (mouseX > 475 && mouseX < 575 && mouseY > 200 && mouseY < 300){
     eyeChange ();
    }

  if (mouseX > 475 && mouseX < 575 && mouseY > 300 && mouseY < 400){
    mouthChange ();
   }
}
