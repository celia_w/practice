![](Screenshot_2021-02-10_at_22.28.41.png)


**References:** 

[disco dressup by mnk2970 as inital inspiration](https://editor.p5js.org/mnk2970/sketches/xLvYxDhFP)

[Sine Jensen's miniX1 project as secondary inspiration ](https://gitlab.com/sine.bj44/aesthetic-programming/-/tree/master/MiniX1)

**Link:** [Click here to try it out!](https://celia_w.gitlab.io/practice/projects/emojiMaker/)


emojiMaker is an attempt to practice: adding custom images to code, as well as clicking one image to change the output (one button containing 3 different images that change between themselves).

Additionally, a practice in figuring out x,y coordinates better for the button placements (by using a rect() with x, y, h, w to figure out what to write for the button placement).

All in all, pretty happy with the result. Further practice would include 1 "other" button, but which displays 3 different images in 3 different placements. 
